# Plant Ranges

### Development

navigate to app folder and develop as usuall

### Dockerizing

When addint new library it needs to be added to Dockerfile.

f.e. installing rgdal:
```dockerfile
RUN R -e "install.packages('rgdal', dependencies = TRUE)"
```

navigate to top level folder and run 

```bash
docker build --tag plants .
```
To run it on your local run
```bash
docker run -d --name plants -p 8181:3838 plants
```

and open webbrowser in 
http://localhost:8181

### Deployment

TBC
still dows nto work automtically
